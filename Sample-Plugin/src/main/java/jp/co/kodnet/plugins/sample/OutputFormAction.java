/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.kodnet.plugins.sample;

/**
 *
 * @author Haima
 */

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.opensymphony.xwork.ActionContext;
import java.util.ArrayList; 
import java.util.List; 
   
 public class OutputFormAction extends ConfluenceActionSupport
 {
     private String uname; 
     private String sexValue; 
     private List<String> interestsValue; 

    private final String[] interestsStr = {"野球","サッカー","ラグビー","相撲","テニス","卓球"}; 
    private final String[] sexStr = {"男性","女性"}; 

     @Override 
     public String execute() throws Exception 
     {
        ActionContext context = ActionContext.getContext(); 
        uname = this.getParameterValue(context,"uname");
        if (uname == null || "".equals(uname))
        {
            return ERROR;
        }
        int sex = Integer.parseInt(this.getParameterValue(context,"sex"));  
        String[] interests =getParameterValues(context,"interests"); 
        sexValue = sexStr[sex]; 
        interestsValue = new ArrayList<String>();
        if(interests != null)
        {
            for (String interest : interests) 
            {
                interestsValue.add(interestsStr[Integer.parseInt(interest)]); 
            }
        }
        return SUCCESS;
        } 
   
     public void setUname(String userName)       
     {
         this.uname=userName;
         
     }
     public String getUname()
     {
         return this.uname;
     }
     public void setSexValue(String genderValue)       
     {
         this.sexValue=genderValue;
     }
     public String getSexValue()
     {
         return this.sexValue;
     }
     
     public void setInterestsValue(List<String> values)
     {
         this.interestsValue = values;
     }
     public List<String> getInterestsValue()
     {
         return this.interestsValue;
     }
   private String getParameterValue(ActionContext context, String key) 
   {
        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[]&& 
                ((String[])paramValues).length != 0)
        {
            return ((String[])paramValues)[0];
        } else if (paramValues instanceof String) 
        {
            return (String)paramValues;
        }
        return null;
   }
   
   private String[] getParameterValues(ActionContext context, String key) 
   {
        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[] && 
                            ((String[])paramValues).length != 0) 
        {
            return (String[])paramValues;
        } 
        return new String[0];
   }
 }
